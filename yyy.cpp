
#include "windows.h"  
#include "math.h"   
#include "iostream" 

using namespace std;

int main() {

	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int x, a, b, c;

	printf("Введіть номер місяця x: ");
	scanf_s("%d", &x);

	a = x / 10;
	b = x % 10;
	c = x % 10;

	switch (a)
	{
	case 2: cout << "двадцять "; break;
	case 3: cout << "тридцать "; break;
	case 4: cout << "сорок "; break;
	case 5: cout << "п'ятдесят "; break;
	case 6: cout << "шістдесят "; break;
	}

	switch (b)
	{
	case 1: cout << "один "; break;
	case 2: cout << "два "; break;
	case 3: cout << "три "; break;
	case 4: cout << "чотири "; break;
	case 5: cout << "п'ять "; break;
	case 6: cout << "шість "; break;
	case 7: cout << "сім "; break;
	case 8: cout << "вісім "; break;
	case 9: cout << "дев'ять "; break;
	case 0: cout << ""; break;
	}

	switch (c)
	{
	case 1: cout << "рік"; break;
	case 2: cout << "роки "; break;
	case 3: cout << "роки "; break;
	case 4: cout << "роки "; break;
	case 5: cout << "років "; break;
	case 6: cout << "років "; break;
	case 7: cout << "років "; break;
	case 8: cout << "років "; break;
	case 9: cout << "років "; break;
	case 0: cout << "років"; break;
	}



	system("pause");
	return 0;

}
